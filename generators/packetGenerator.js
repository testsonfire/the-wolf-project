var SharePacket = require('../models/sharePacket');
var companies  = ["Google", "Facebook", "Vk", "Instagram", "Microsoft", "SpaceX", "Intel", "Amazon", "Sony", "IBM"];

var generatePacket = function(io){
    var max = 5, min = -5 ;
    var changed = Math.random() *(max - min) + min;
    var companyId = Math.floor(Math.random() * companies.length);
    var amount = Math.floor(Math.random () * 1000);
    var price = Math.floor(Math.random() * 10000);
    var isInspected = false;
    var isLegal = Math.random() < 0.5;
    var closeTime = Math.floor(Date.now()/1000 + 1800);
    var packet = new SharePacket({
        company: companies[companyId],
        amount: amount,
        price: price,
        isInspected: isInspected,
        changed: changed.toFixed(2),
        isLegal: isLegal,
        isSelling: true,
        closeTime: closeTime
    });
    packet.save(function(err, packet) {
        if(err) {
            throw err;
        }
        if(!packet) return;
        var data = {packet: packet};
        io.sockets.emit('broadcast packet generated', data);
        console.log("data generated");
    });
}

module.exports = generatePacket;