var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AlertSchema = new Schema({
    message: {type: String, required: true},
    date: {type: Date, required: true},
    from: {type: String, required: true},
    to: {type: String, required: true}
}, {
    versionKey: false
});

module.exports = mongoose.model('Alert', AlertSchema);
