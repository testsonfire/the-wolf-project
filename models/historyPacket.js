var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var HistoryPacketSchema = new Schema({
    packetId: {type: String, required: true},
    sellerId: {type: String, required: true},
    company: {type: String, required: true},
    price: {type: Number, required: true},
    amount: {type: Number, required: true},
    changed: {type: Number, required: true},
    isLegal: {type: Boolean, default: true},
    buyerName: {type: String, required: true},
    date: {type: String, required: true},
    
}, {
    versionKey: false
});

module.exports = mongoose.model('HistoryPacket', HistoryPacketSchema);