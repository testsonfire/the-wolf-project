var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var User = require('./user');

var SharePacketSchema = new Schema({
    company: {type: String, required: true},
    amount: {type: Number, required: true},
    price: {type: Number, required: true},
    changed: {type: Number, required: true}, //positive: up, negative: down
    isInspected: {type: Boolean, default: false},
    isLegal: {type: Boolean, default: true},
    isSelling: {type: Boolean, default: true},
    ownerId: {type: Schema.Types.ObjectId, ref: "User", default: null},
    closeTime: {type: Number, required: true},
    wasInspected: {type: Boolean, default: false}
}, {
    versionKey: false
});

module.exports = mongoose.model('SharePacket', SharePacketSchema);
