
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AdvertisementSchema = new Schema({
    comment: {type: String, required: true},
    sharePacket: {type:Schema.Types.ObjectId , ref: 'SharePacket'},
    sellerId: {type: String, default: null}
}, {
    versionKey: false
});

module.exports = mongoose.model('Advertisement', AdvertisementSchema);