
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    username: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},
    role: {type: String, required: true},
    balance: {type: Number, default: 100000},
    mixed: {type: Schema.Types.Mixed}
}, {
    versionKey: false
});

UserSchema.virtual('id').get(function(){
    return this._id;
});

module.exports = mongoose.model('User', UserSchema);