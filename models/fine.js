var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FineSchema = new Schema({
    from: {type: String, require: true},
    amount: {type: Number, required: true},
    to: {type: String, required: true},
    comment: {type: String}
}, {
    versionKey: false
});

module.exports = mongoose.model('Fine', FineSchema);
