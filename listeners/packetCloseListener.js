var SharePacket = require('../models/sharePacket');

var packetCloseListener = function(io){
    SharePacket.find({}, function(err, packets){
        if(err) throw err;
        Promise.all(packets.map(function(packet){
            return new Promise(function(resolve, reject){
                if(packet.isSelling){
                    var now = Math.floor(Date.now()/1000);
                    var remainingTime =  packet.closeTime - now;
                    if(remainingTime <= 0){
                        var closeTime = now  +  1800;
                        packet.closeTime = closeTime;
                        packet.price = Math.floor(packet.price*packet.changed/100 + packet.price);
                        if(packet.price < 0) {
                            packet.price = 1;
                            packet.changed = 1.0;
                        }
                        packet.save(function(err, packet){
                            if(err) {
                                reject(err);
                                return;
                            }
                            io.sockets.emit("broadcast close time changed", packet);
                            resolve(packet);
                        });
                    }
                }
            });
        })).then(function(data){
            //TODO: what to do with this data
            //console.log(data);
        });
    })
};

module.exports = packetCloseListener;
