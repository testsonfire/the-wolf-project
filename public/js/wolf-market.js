var wolfMainViewModel = null;
var wolfMarketViewModel = null;
var wolfHistoryViewModel = null;


function Advertisement(packet) {
    var self = this;

    self.company = packet.company;
    self._id = packet._id;
    self.amount = ko.observable(packet.amount);
    self.price = ko.observable(packet.price);
    self.changed = ko.observable(packet.changed);
    self.isInspected = ko.observable(packet.isInspected);
    self.isLegal = ko.observable(packet.isLegal);
    self.isSelling = ko.observable(packet.isSelling);
    self.ownerId = ko.observable(packet.ownerId);
    self.remainingTime = ko.observable("");
    self.closeTime = ko.observable(Math.floor(Date.now()/1000));
    self.timerId = ko.observable(0);
}

function WolfMarketViewModel() {
    var self = this;
    
    self.advertisements = ko.observableArray([]);
    self.userId = ko.observable(localStorage.userId);

    self.username = localStorage.username;
    self.balance = ko.observable();

    //TODO: OBSERVE WOLF'S BALANCE AND DEACTIVATE BUTTON BUY WHEN BALANCE IS LESS THAN PACKET'S PRICE
    self.takeSelectedPacketOff = function(advertisement){
        $.post('/api/advertisements/take-off',{
            packetId: advertisement._id,
            token: localStorage.accessToken
        }).done(function(data){
            if(data.success) {
                self.advertisements.remove(advertisement);
            }
        });
    }

    self.buySelectedPacket = function(advertisement){
        $.post('/api/wolf/purchase-shares-packet', {
            packetId: advertisement._id,
            token: localStorage.accessToken
        }).done(function(data) {
            if(data.success) {
                self.advertisements.remove(advertisement);
                self.balance(data.balance);
            }else {
                alert("Failed. " + data.message);
            }
        });
    }

    self.findPacketById = function(packetId) {
        return ko.utils.arrayFirst(self.advertisements(), function(advertisement) {
            return advertisement._id === packetId;
        });
    }

    self.appendPacket = function(packet){
        var remainingTime = packet.closeTime - Math.floor(Date.now()/1000);
        packet = new Advertisement(packet);
        self.advertisements.push(packet);
        var timerId = setInterval(function(){
            if(remainingTime >=0){
                packet.remainingTime(constructTime(remainingTime));
            }else {
                clearInterval(timerId);
            }
            remainingTime--;
        }, 1000);
        packet.timerId(timerId);
    }
}

wolfMarketViewModel = new WolfMarketViewModel();

ko.applyBindings(wolfMarketViewModel);

$(function() {

    $.ajax({
        url: '/api/advertisements',
        headers: {
            'x-access-token': localStorage.accessToken
        },
        success: function (packets) {
            $(packets).each(function(i, packet){
                wolfMarketViewModel.appendPacket(packet);
            });
        },
        error: function (error) {
        }
    });

    $.ajax({
        url: '/api/wolf/balance',
        headers: {
            'x-access-token': localStorage.accessToken
        },
        success: function (data) {
            if(data.success){
                wolfMarketViewModel.balance(data.balance);
            }else {
                wolfMarketViewModel.balance(0);
            }
        },
        error: function (error) {
        }
    });

});
