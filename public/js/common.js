
var NOTIFY_DELAY = 5000;

function logout() {
    $.post('/logout', {
        userId: localStorage.getItem('username')
    }).done(function(data){
        localStorage.removeItem('accessToken');
        localStorage.removeItem('userId');
        localStorage.removeItem('username');
        localStorage.removeItem('email');
        localStorage.removeItem('role');
        window.location.href="/";
        console.log(data);
    });
}

function constructTime(seconds) {
    var h = Math.floor(seconds/3600);
    var m = Math.floor((seconds%3600)/60);
    var s = Math.floor((seconds%3600)%60);

    if(h < 10) {
        h = '0' + h;
    }
    if(m < 10) {
        m = '0' + m ;
    }
    if(s < 10) {
        s = '0' + s;
    }
    var result = h+':'+m +':' + s;
    return  result;
}


