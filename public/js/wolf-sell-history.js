var wolfMainViewModel = null;
var wolfMarketViewModel = null;
var wolfHistoryViewModel = null;

function HistoryPacket(packet) {
    var self = this;

    self._id = packet._id;
    self.company = packet.company;
    self.price = packet.price;
    self.amount = packet.amount;
    self.changed = packet.changed;
    self.isLegal = packet.isLegal;
    self.buyerName = packet.buyerName;
    self.date = packet.date;
    
}

function WolfHistoryViewModel() {
    var self = this;

    self.historyPackets = ko.observableArray([]);

    self.balance = ko.observable();
    
    self.username = localStorage.username;

}

wolfHistoryViewModel = new WolfHistoryViewModel();

ko.applyBindings(wolfHistoryViewModel);

$(function () {
    /*--------------------------AJAX REQUEST PROCESSING--------------------------*/
    $.ajax({
        url: '/api/wolf/sell-history',
        headers: {
            'x-access-token': localStorage.accessToken
        },
        success: function (packets) {
            $(packets).each(function (i, packet) {
                wolfHistoryViewModel.historyPackets.push(new HistoryPacket(packet));
            });
        },
        error: function (error) {
        }
    });

    $.ajax({
        url: '/api/wolf/balance',
        headers: {
            'x-access-token': localStorage.accessToken
        },
        success: function (data) {
            if (data.success) {
                wolfHistoryViewModel.balance(data.balance);
            } else {
                wolfHistoryViewModel.balance(0);
            }
        },
        error: function (error) {
        }
    });

});