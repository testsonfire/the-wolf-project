function storeUserProfile(user){
    window.localStorage.accessToken = user.token;
    window.localStorage.userId = user.userId;
    window.localStorage.username = user.username;
    window.localStorage.email = user.email;
    window.localStorage.role = user.role;
}

$('.sign-up-form').validator().on('submit', function (e) {

    if (e.isDefaultPrevented()) {
        // handle the invalid form...
    } else {
        e.preventDefault();
        $.post('/register', {
            username: $('#username').val(),
            email: $('#email').val(),
            password: $('#password').val(),
            role: $('#role-pick').val()
        }).done(function (result) {
            if(result.success){
                storeUserProfile(result);
                window.location.replace(result.redirect);
            } else {
                if($("#authentication-alert").is(":visible")) {
                    $("#authentication-alert").toggle();
                }

                $("#authentication-alert").toggle();
                $("#authentication-alert-content").text(" " + result.message);
            }
        });
    }
});

$('.login-form').validator().on('submit', function (e) {

    if (e.isDefaultPrevented()) {
        // handle the invalid form...
    } else {
        e.preventDefault();
        $.post('/login', {
            email: $('#login-email').val(),
            password: $('#login-password').val()
        }).done(function(result){
            if(result.success){
                storeUserProfile(result);
                window.location.replace(result.redirect);

            } else {
                
                if($("#authentication-alert").is(":visible")) {
                    $("#authentication-alert").toggle();
                }

                $("#authentication-alert").toggle();
                $("#authentication-alert-content").text(" " + result.message);
            }
        });
    }
});


$('.message a').click(function () {
    if($("#authentication-alert").is(":visible")) {
        $("#authentication-alert").toggle();
    }
    $('form').animate({ height: "toggle", opacity: "toggle" }, "slow");
});


$(function() {
    $("#register-button").prop( "disabled", true );
});

$(':checkbox').change(function() {

    if(!this.checked) {
        $("#register-button").prop( "disabled", true );
    } else {
        $("#register-button").prop( "disabled", false );
    }

}); 