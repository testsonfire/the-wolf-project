$(function(){
    var copViewModel = null;

    function WolfPacket(wolfname, packet) {
        var self = this;

        self.wolfname = ko.observable(wolfname);
        self.company = ko.observable(packet.company);
        self.amount = ko.observable(packet.amount);
        self.price = ko.observable(packet.price);
        self.changed = ko.observable(packet.changed);
        self.isInspected = ko.observable(packet.isInspected);
        self.isLegal = ko.observable(packet.isLegal);
        self.isSelling = ko.observable(packet.isSelling);
        self.ownerId = ko.observable((packet.ownerId));
        self._id = ko.observable(packet._id);
        self.wasInspected = ko.observable(packet.wasInspected);
        self.remainInspectionTime = ko.observable();
        self.done = ko.observable(false);
    }

    var CopViewModel = function() {
        var self = this;

        self.isInspecting = ko.observable(false);
        self.copId = localStorage.userId;
        self.username = localStorage.username;
        self.reputation = ko.observable(0);
        self.wolfPackets = ko.observableArray([]);
        self.wolfToBeFined = ko.observable("");
        self.packetIdToBeFined = ko.observable("");

        self.inspectPacket  = function(wolfPacket) {
            $.post('/api/cop/inspect', {
                packetId: wolfPacket._id,
                token: localStorage.accessToken
            }).done(function(data){
                if(data.success) {
                    self.isInspecting(true);
                    wolfPacket.isInspected(true);
                    var remainingTime = data.inspectionDuration;
                    //launch a timer for inspection
                    var timerId = setInterval(function(){
                        if(remainingTime >=0){
                            wolfPacket.remainInspectionTime (constructTime(remainingTime) +  " до конца проверки");
                        }else {
                            clearInterval(timerId);
                            self.finishInspection(wolfPacket);
                        }
                        remainingTime--;
                    }, 1000);
                }
            });
        }

        self.finishInspection = function(wolfPacket) {
            $.post('/api/cop/finish-inspect', {
                packetId: wolfPacket._id,
                token: localStorage.accessToken
            }).done(function(data){
                self.isInspecting(false);
                wolfPacket.isInspected(false);
                wolfPacket.isLegal(data.isLegal);
                wolfPacket.wasInspected(true);
                wolfPacket.done(true);
                if(data.isLegal){
                    $.notify({
                        icon: 'fa fa-level-down',
                        message: 'Внимание! Пакет акций оказался ликивдным, ваша репутация понизилась.',
                        url: '/cop',
                        target: "_self"
                    }, {
                            delay: NOTIFY_DELAY,
                            type: 'danger',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            }

                        });
                }else {
                    $.notify({
                        icon: 'fa fa-level-up',
                        message: 'Внимание! Пакет акций оказался неликивдным, ваша репутация повысилась.',
                        url: '/cop',
                        target: "_self"
                    }, {
                            delay: NOTIFY_DELAY,
                            type: 'success',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            }

                        });
                }
                //update cop's reputation
                console.log(data);
                copViewModel.reputation(data.reputation);
            });
        }



        self.createFine = function(wolfPacket) {
            self.wolfToBeFined(wolfPacket.wolfname());
            self.packetIdToBeFined(wolfPacket._id());
            $("#fine-value").val(wolfPacket.price()/2);
        }

        self.postFine = function(){
            var id = self.packetIdToBeFined();
            var fine = self.findPacketById(id).price() / 2;
            $.post('/api/cop/fines', {
                from: "FBI",
                to: self.wolfToBeFined(),
                amount: fine,
                packetId: self.packetIdToBeFined(),
                comment: $("#fine-reason option:selected").text(),
                token: localStorage.accessToken
            }).done(function(data){
                var packet = self.findPacketById(id);
                self.wolfPackets.remove(packet);
                self.reputation(data.reputation);
            });
        }

        self.findPacketById = function(packetId) {
            // console.log(packetId);
            return ko.utils.arrayFirst(self.wolfPackets(), function(wolfPacket) {
                // console.log(wolfPacket);
                return wolfPacket._id() === packetId;
            });
        }
    }

    copViewModel = new CopViewModel();

    ko.applyBindings(copViewModel);

    /*--------------------------SOCKET IO PROCESSING--------------------------*/
    var socket = io.connect();
    socket.emit('join', {username: window.localStorage.username});

    //
    socket.on('broadcast packet added to market', function(data) {
        console.log(data);
        copViewModel.wolfPackets.push(new WolfPacket(data.wolfname, data.packet));
        
        $.notify({
            icon: 'fa fa-exclamation',
            message: 'Волк ' + data.wolfname + ' выставил на Фондовый рынок пакет акций компании ' + data.packet.company + '\
                      по цене $' + data.packet.price + ' в количестве ' + data.packet.amount + ' акций.',

            url: '/wolf/market',
            target: "_self"
        }, {
                delay: NOTIFY_DELAY,
                type: 'info',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
            });
    });

    socket.on('broadcast packet taken off from market', function(data) {
        var wolfPacket = copViewModel.findPacketById(data.packetId);
        copViewModel.wolfPackets.remove(wolfPacket);

        $.notify({
            icon: 'fa fa-exclamation',
            message: 'Волк ' + wolfPacket.wolfname() + ' снял с продажи пакет акций компании ' + wolfPacket.company() + '\
                      по цене $' + wolfPacket.price() + ' в количестве ' + wolfPacket.amount() + ' акций.',

            url: '/wolf/market',
            target: "_self"
        }, {
                delay: NOTIFY_DELAY,
                type: 'info',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
            });
    });

    socket.on('broadcast wolf bought packet', function(data) {
        var wolfPacket = copViewModel.findPacketById(data.packetId);
        if(wolfPacket != null) {
            copViewModel.wolfPackets.remove(wolfPacket);
            $.notify({
                icon: 'fa fa-exclamation',
                message: 'Волк ' + wolfPacket.wolfname() + ' купил пакет акций компании ' + wolfPacket.company() + '\
                        по цене $' + wolfPacket.price() + ' в количестве ' + wolfPacket.amount() + ' акций.',
                url: '/wolf/market',
                target: "_self"
            },{
                delay: NOTIFY_DELAY,
                type: 'info',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
            });
        } else {
            $.notify({
                icon: 'fa fa-exclamation',
                message: 'Волк ' + data.customerName + ' купил пакет акций у системы',
                url: '/wolf/market',
                target: "_self"
            }, {
                delay: NOTIFY_DELAY,
                type: 'info',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
            });
        }
    });

    socket.on('unicast wolf bankrupted', function(data){
        $.notify({
            icon: 'fa fa-gavel',
            message: 'Внимание! Волк ' + data.username + ' теперь банкрот!',
            url: '/cop',
            target: "_self"
        }, {
            type: 'success',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            }

        });
    });

    socket.on('broadcast wolf bankrupted', function(data){
        $.notify({
            icon: 'fa fa-gavel',
            message: 'Внимание! Волк ' + data.username + ' теперь банкрот!',
            url: '/cop',
            target: "_self"
        }, {
            type: 'success',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            }

        });
    });

    socket.on('broadcast cop lost reputation', function(data) {
        $.notify({
            icon: 'fa fa-exclamation',
            message: 'Внимание, + '  + data.copname + '. Ваша репутация упала ниже 0.' ,
            url: '/cop',
            target: "_self"
        }, {
            delay: NOTIFY_DELAY,
            type: 'danger',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            }
        });
    });

    /*--------------------------AJAX REQUEST PROCESSING--------------------------*/


    $.ajax({
        url: '/api/cop/wolf-public-packets',
        headers: {
            'x-access-token': localStorage.accessToken
        },
        success: function (data) {
            $.each(data.wolvesPackets, function(i, wolfPackets){
                $.each(wolfPackets.sellingPackets, function(i, packet){
                    copViewModel.wolfPackets.push(new WolfPacket(wolfPackets.username, packet));
                });
            });
        },
        error: function (error) {
        }
    });

    $.ajax({
        url: '/api/cop/reputation',
        headers: {
            'x-access-token': localStorage.accessToken
        },
        success: function (data) {
            copViewModel.reputation(data.reputation);
        },
        error: function (error) {
        }
    });
});