
$(function(){
    /*--------------------------SOCKET IO PROCESSING--------------------------*/
    var socket = io.connect();
    socket.emit('join', {username: window.localStorage.username});

    //To change the stock market status, apply for all wolf when cop starts inspecting a packet
    socket.on('broadcast packet inspection', function(data){
        if(data.ownerId != localStorage.userId) {
            $.notify({
                icon: 'fa fa-exclamation',
                message: 'Внимание! Полицейский начал проверять пакет акций на Фондовом рынке.',
                url: '/wolf/market',
                target: "_self"
            }, {
                    delay: NOTIFY_DELAY,
                    type: 'danger',
                    animate: {
                        enter: 'animated bounceIn',
                        exit: 'animated bounceOut'
                    }

                });
        }
        if(wolfMarketViewModel){
            var packet = wolfMarketViewModel.findPacketById(data.id);
            if(packet){
                packet.isInspected(true);
            }
        }
        if(wolfMainViewModel){
            var packet = wolfMainViewModel.findPacketById(data.id);
            if(packet){
                packet.isInspected(true);
            }
        }
    });


    //To change the stock market status, apply for all wolf when cop stop inspecting a packet
    socket.on('broadcast packet inspection finished', function(data){
        if(data.ownerId != localStorage.userId){
            $.notify({
                icon: 'fa fa-exclamation',
                message: 'Внимание! Полицейский завершил проверку пакета акций на Фондовом рынке.',
                url: '/wolf/market',
                target: "_self"
            }, {
                    delay: NOTIFY_DELAY,
                    type: 'danger',
                    animate: {
                        enter: 'animated bounceIn',
                        exit: 'animated bounceOut'
                    }

                });
        }
        if(wolfMarketViewModel) {
            var packet = wolfMarketViewModel.findPacketById(data.packetId);
            if(packet) {
                packet.isInspected(false);
            }
        }
        if(wolfMainViewModel){
            var packet = wolfMainViewModel.findPacketById(data.packetId);
            if(packet) {
                packet.isInspected(false);
            }
        }
    });

    socket.on('unicast packet inspection', function(data) {
        
        $.notify({
            icon: 'fa fa-exclamation',
            message: 'Внимание! Полицейский проверяет ваш пакет акций',// + ' ('+ data.packet.company + ', $ '+ data.packet.price +') ' + '.',
            url: '/wolf',
            target: "_self" 
        }, {
                delay: NOTIFY_DELAY,
                type: 'danger',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
	
            });
    });


    socket.on('unicast packet inspection finished', function(data) {
       
        $.notify({
            icon: 'fa fa-exclamation',
            message: 'Внимание! Полицейский завершил проверку вашего пакета акций',// + ' ('+ data.packet.company + ', $ '+ data.packet.price +') '+  '.',
            url: '/wolf',
            target: "_self" 
        }, {
                delay: NOTIFY_DELAY,
                type: 'warning',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
	
            });
    });

    socket.on('broadcast packet generated', function(data){
        $.notify({
            icon: 'fa fa-exclamation',
            message: 'Новый пакет акций компании ' + data.packet.company + ' выставлен на продажу.',
            url: '/wolf/market',
            target: "_self" 
        }, {
                delay: NOTIFY_DELAY,
                type: 'info',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
            });
        if(wolfMarketViewModel){
            wolfMarketViewModel.appendPacket(data.packet);
        }
    });
    //
    socket.on('broadcast packet added to market', function(data) {
        if(wolfMarketViewModel){
            console.log("From market ");
            console.log(data.packet);
            wolfMarketViewModel.appendPacket(data.packet);
        }
    });

    socket.on('unicast packet added to market', function(data) {
        if(wolfMainViewModel){
            //TODO: update status of button/text of wolf's page
            //wolfMainViewModel.advertisements.push(new Advertisement(data.packet));
        }
    });

    socket.on('broadcast packet taken off from market', function(data) {
        if(wolfMarketViewModel) {
            var packet  = wolfMarketViewModel.findPacketById(data.packetId);
            wolfMarketViewModel.advertisements.remove(packet);
        }
    });

    socket.on('unicast packet taken off from market', function(data){
        //data content packet's id
        //TODO: find packet by id from wolfMainViewModel then change state
        if(wolfMainViewModel) {
            $.notify({
                icon: 'fa fa-exclamation',
                message: 'Один из ваших пакетов акций убран с фондового рынка.',
                url: '/wolf/market',
                target: "_self"
            }, {
                    delay: NOTIFY_DELAY,
                    type: 'info',
                    animate: {
                        enter: 'animated bounceIn',
                        exit: 'animated bounceOut'
                    }
                });
        }
    });

    socket.on("broadcast close time changed", function(data){
        //TODO: display notification about packet's price has changed, object data: packet
        if(wolfMarketViewModel) {
            var packet = wolfMarketViewModel.findPacketById(data._id);
            if(packet) {
                packet.closeTime(data.closeTime);
                packet.price(data.price);
                packet.changed(data.changed);
                var remainingTime = packet.closeTime() - Math.floor(Date.now()/1000);
                clearInterval(packet.timerId());
                var timerId = setInterval(function(){
                    if(remainingTime >=0){
                        packet.remainingTime(constructTime(remainingTime));
                    }else {
                        clearInterval(timerId);
                    }
                    remainingTime--;
                }, 1000);
                packet.timerId(timerId);

                console.log(data);
            }
        }
    });

    socket.on('unicast seller packet sold', function(data){
        //update balance
        if(wolfMainViewModel){
            wolfMainViewModel.packets.remove(wolfMainViewModel.findPacketById(data.packetId));
            wolfMainViewModel.balance(data.balance);
        }
        if(wolfMarketViewModel){
            wolfMainViewModel.balance(data.balance);
        }
        
        $.notify({
            icon: 'fa fa-exclamation',
            message: 'Ваш пакет акций купили на фондовом рынке',
            url: '/wolf/market',
            target: "_self"
        }, {
                delay: NOTIFY_DELAY,
                type: 'info',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
            });
    });

    socket.on('broadcast packet removed by cop', function(data){
        console.log(data);
        if(wolfMarketViewModel) {
            var packet = wolfMarketViewModel.findPacketById(data.packetId);
            if(packet) {
                wolfMarketViewModel.advertisements.remove(packet);
            }
        }
        if(wolfMainViewModel) {
            var packet = wolfMainViewModel.findPacketById(data.packetId);
            if(packet) {
                wolfMainViewModel.packets.remove(packet);
            }
        }
    });

    socket.on('unicast fine created',function(data) {
        console.log(data.balance);
        if(wolfMarketViewModel){
            wolfMarketViewModel.balance(data.balance);
        }else if(wolfMainViewModel){
            wolfMainViewModel.balance(data.balance);
        }

        console.log(data);


        $.notify({
            icon: 'fa fa-exclamation',
            message: 'Внимание! Вам был выписан штраф в размере $' + data.fine.amount + '.',
            url: '/wolf',
            target: "_self"
        }, {
                delay: NOTIFY_DELAY,
                type: 'danger',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
        });
    });

    socket.on('broadcast wolf bought packet', function(data){
        console.log(data);
        if(wolfMarketViewModel) {
            var packet = wolfMarketViewModel.findPacketById(data.packetId);
            if (packet != null) {
                wolfMarketViewModel.advertisements.remove(packet);
                if(data.customerName != localStorage.username){
                    //add notification for other wolves
                }
            }
        }
    });

    socket.on('broadcast wolf bankrupted', function(data) {
        console.log("wolf bankrupted");
        if(data.username != localStorage.username) {
            $.notify({
                icon: 'fa fa-exclamation',
                message: 'Внимание! ' + data.username + " теперь банкрот!",
                url: '/wolf',
                target: "_self"
            }, {
                delay: NOTIFY_DELAY,
                type: 'success',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
            });
        }else  {
            $.notify({
                icon: 'fa fa-exclamation',
                message: 'Внимание! Вы теперь банкрот!!! ' ,
                url: '/wolf',
                target: "_self"
            }, {
                delay: NOTIFY_DELAY,
                type: 'danger',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                }
            });
        }
    });

    socket.on('broadcast cop lost reputation', function(data) {
        $.notify({
            icon: 'fa fa-exclamation',
            message: 'Внимание! Репутация полицейского ' + data.copname +   ' ниже 0.' ,
            url: '/wolf',
            target: "_self"
        }, {
            delay: NOTIFY_DELAY,
            type: 'info',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            }
        });
    });

});
