var wolfHistoryViewModel = null;
var wolfMarketViewModel = null;
var wolfMainViewModel = null;

function Packet(packet) {
    var self = this;

    self._id = packet._id;
    self.company = packet.company;
    self.amount = ko.observable(packet.amount);
    self.price = ko.observable(packet.price);
    self.changed = ko.observable(packet.changed);
    self.isInspected = ko.observable(packet.isInspected);
    self.isLegal = ko.observable(packet.isLegal);
    self.isSelling = ko.observable(packet.isSelling);
}

function WolfMainViewModel() {
    var self = this;
    self.packets = ko.observableArray([]);

    self.balance = ko.observable('');
    self.username = localStorage.username;

    self.findPacketById = function(packetId) {
        return ko.utils.arrayFirst(self.packets(), function(packet) {
            return packet._id === packetId;
        });
    }

    self.addSelectedPacketToMarket = function(packet) {
        $.post('/api/advertisements',{
            packetId: packet._id,
            token: localStorage.accessToken,
        }).done(function(data){
            if(data.success) {
                packet.isSelling(true);
            }
        });
    }
}

wolfMainViewModel = new WolfMainViewModel();

ko.applyBindings(wolfMainViewModel);

$(function(){
    /*--------------------------AJAX REQUEST PROCESSING--------------------------*/
    $.ajax({
        url: '/api/wolf/shares',
        headers: {
            'x-access-token': localStorage.accessToken
        },
        success: function (packets) {
            $(packets).each(function(i, packet) {
                wolfMainViewModel.packets.push(new Packet(packet));
            });
        },
        error: function (error) {
        }
    });

    $.ajax({
        url: '/api/wolf/balance',
        headers: {
            'x-access-token': localStorage.accessToken
        },
        success: function (data) {
            if(data.success){
                wolfMainViewModel.balance(data.balance);
            }else {
                wolfMainViewModel.balance(0);
            }
        },
        error: function (error) {
        }
    });

});