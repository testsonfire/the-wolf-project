/*--------------------REQUIRE MODULES-----------------------*/
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var acl = require('express-acl');
var usersRoute = require('./routes/userRoute');
var basicRoute = require('./routes/basicRoute');
var auth = require('./routes/authRoute');
var logoutRoute  = require('./routes/logoutRoute');
var register = require('./routes/registerRoute');
var authMiddleware = require('./routes/authMiddleware');
var wolfRoute = require('./routes/wolfRoute');
var copRoute = require('./routes/copRoute');
var advertisementRoute = require('./routes/advertisementRoute');
var mongoose = require('mongoose');
var config = require('./config');
var packetGenerator = require('./generators/packetGenerator');
var packetCloseListener = require('./listeners/packetCloseListener');
var logger = require('morgan');
var cookieParser = require('cookie-parser');

/*--------------------CREATE APP-----------------------*/
var app = express();
/*--------------------START SERVER-----------------------*/
var debug = require('debug')('wolf-cop:server');
var http = require('http');

var port = process.env.PORT || '3000';
app.set('port', port);

var server = http.createServer(app);
var io = require('socket.io').listen(server);
app.io = io;
server.listen(port, function(){
  console.log('server is running on port '  + port);
});
/*--------------------GENERATE RANDOM PACKET---------------------*/
setInterval(function(){
  packetGenerator(io);
}, 60000);

setInterval(function(){
  packetCloseListener(io);
}, 2000);

/*--------------------SERVER ERROR HANDLER-----------------------*/
server.on('error', function(error){
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port;

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
});
/*--------------------DATABASE CONNECTION------------*/
mongoose.connect(config.database);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

/*--------------------APP'S SECRET SETUP------------*/
app.set('superSecret', config.secret);

/*--------------------VIEW ENGINE SETUP-------------*/
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
/*--------------------SOCKET DELEGATION-------------*/
app.use(function(req, res, next){
  req.io = io;
  next();
});

app.use('/', basicRoute);
app.use('/login', auth);
app.use('/logout', logoutRoute);
app.use('/register', register);
app.use('/api/', [authMiddleware]);
app.use('/api/users', usersRoute);
app.use('/api/wolf', wolfRoute);
app.use('/api/cop', copRoute);
app.use('/api/advertisements', advertisementRoute);
/*--------------------CLIENT ERROR HANDLER------------*/
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
/*--------------------ERROR HANDLER------------------*/
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
/*--------------------SOCKET.IO PROCESSING------------*/
io.sockets.on('connection', function(socket) {
  console.log('Client connected to socket server.');
  socket.on('join', function(data){
    socket.join(data.username);
    console.log("Client " + data.username + " Joined");
  });

});
/*-------------------SERVER STATIC FILES*/
app.use(express.static(__dirname + '/public'));

module.exports = app;//for testing
