var express = require('express');
var router = express.Router();
var SharePacket = require('../models/sharePacket');
var Alert = require('../models/alert');
var config = require('../config');
var acl = require('express-acl');
var mongoose = require('mongoose');
var Fine = require('../models/fine');
var User = require('../models/user');
acl.config({
    filename: 'nacl.json',
    baseUrl: 'api'
});

router.use(acl.authorize);

router.route('/inspect')
    .post(function(req, res) {
        SharePacket.findOne({_id: req.body.packetId, isInspected: false}, function(err, packet) {
            if(err) {throw err}
            if(!packet){
                res.json({success: false, message: "Пакет акций был продан или снят с продаж на фондовом рынке."});
            }else {
                if(!packet.isInspected) {
                    packet.isInspected = true;
                    packet.save(function(err, packet) {
                        if(packet.ownerId) {
                            req.io.sockets.emit('broadcast packet inspection', {id: packet._id, ownerId: packet.ownerId});
                            alert = new Alert({
                                message: "Ваш пакет акций с id = " + packet._id + " проверяется полицейским." ,
                                date: new Date(),
                                from: req.userId,
                                to: packet.ownerId
                            });
                            alert.save(function(err) {
                                if(err){throw err; }
                                User.findOne({_id: packet.ownerId}, function(err, user){
                                    if(err) throw err;
                                    if(!user) return;
                                    req.io.sockets.in(user.username).emit('unicast packet inspection', alert);
                                    User.findOne({role: 'cop'}, function(err, cop) {
                                        if(err) throw err;
                                        if(!cop) res.json({});
                                        var inspectionDuration = 5;
                                        if(cop.mixed.reputation <= 0) {
                                            inspectionDuration = 20;
                                        }
                                        console.log(inspectionDuration);
                                        res.json({success: true, inspectionDuration:inspectionDuration , message:"Вы проверяете пакет акций с id " + packet._id + " пользователя " +  user.username});
                                    })
                                });
                            });
                        }else {
                            res.json({success: false, message: "Вы не можете проверить системный пакет акций."});
                        }
                    });
                }else {
                    res.json({success: false, message: "Кто-то проверяет этот пакет акций."});
                }
            }
        });
    });

router.route('/finish-inspect')
    .post(function(req, res) {
        var packetId = req.body.packetId;
        SharePacket.findOne({_id: packetId, isInspected: true}, function (err, packet) {
            if (err) throw err;
            if (!packet) {
                res.json({success: false, message: "Пакет акций не найден."});
            } else {
                if(packet.isInspected) {
                    packet.wasInspected = true;
                    packet.isInspected = false;
                    packet.save(function(err, packet) {
                        User.findOne({_id: packet.ownerId}, function(err, user) {
                            if(err) throw err;
                            if(!user) res.json({success: false, message: "Владелей пакета акций не найден."});
                            var inspectionResult = packet.isLegal ? " valid" : " invalid";
                            alert = new Alert({
                                message: "Пакет акций с id = " + packetId + " был проверен с результатом " + inspectionResult,
                                date: new Date(),
                                from: req.username,
                                to: user.username
                            });
                            alert.save(function (err) {
                                if (err) {
                                    throw err;
                                }
                                User.findOne({role: "cop"}, function(err, cop){
                                    if(err) throw err;
                                    if(!cop) return;
                                    if(packet.isLegal){
                                        cop.mixed.reputation -= 10;
                                    }else {
                                        cop.mixed.reputation += 10;
                                    }
                                    if(cop.mixed.reputation <=0 ) {
                                        req.io.sockets.emit('broadcast cop lost reputation', {copname: cop.username});
                                    }
                                    cop.markModified('mixed');
                                    cop.save(function(err, cop){
                                        if(err) throw err;
                                        if(!cop) return;
                                        req.io.sockets.emit('broadcast packet inspection finished', {packetId: packet._id, isLegal: packet.isLegal, ownerId: packet.ownerId});
                                        req.io.sockets.in(user.username).emit('unicast packet inspection finished', {msg: alert});
                                        res.json({success: true, isLegal: packet.isLegal, reputation: cop.mixed.reputation, message:"Проверка пакета акций завершена."});
                                    });
                                });
                            });
                        });
                    });
                }else {
                    res.json({success: false, message: "Проверка пакета акций уже производилась ранее."});
                }
            }
        });
    });

router.route('/fines')
    .get(function(req, res){
        Fine.find({}, function(err, fines){
            if(err){
                throw err;
            }
            res.json(fines);
        });
    })
    .post(function(req, res){
        var fine = new Fine(req.body);
        fine.save(function(err, fine){
            if(err) {throw err;}
            User.findOne({username: req.body.to}, function(err, user){
                if(err) throw err;
                if(user) {
                    var shouldEmitBankrupt = user.balance > 0;
                    user.balance -=  req.body.amount;
                    user.save(function(err, user) {
                        if(err) throw err;
                        //remove packet from system
                        SharePacket.remove({_id: req.body.packetId}, function(err){
                            if(err) {
                                throw err;
                            }else {
                                //change reputation of use
                                User.findOne({role: "cop"}, function(err, cop){
                                    if(err) {
                                        throw err;
                                    }
                                    if(!cop){
                                        return;
                                    }
                                    // cop.mixed.reputation +=  10;
                                    cop.save(function(err, cop){
                                        if(err) throw err;
                                        req.io.sockets.in(user.username).emit('unicast fine created', {fine: fine, balance: user.balance});
                                        req.io.sockets.emit("broadcast packet removed by cop", {packetId: req.body.packetId});
                                        if(user.balance<=0 && shouldEmitBankrupt) {
                                            req.io.sockets.emit("broadcast wolf bankrupted", {username: user.username});
                                            req.io.sockets.in(cop.username).emit('unicast wolf bankrupted', {username: user.username});
                                        }

                                        res.json({success: true, message: "Баланс пользователя был изменен.", reputation: cop.mixed.reputation});

                                    });

                                });
                            }
                        });
                    });
                }else {
                    res.json({success: false, message: "Пользователь не найден."});
                }
            });

        });
    });

router.route('/wolf-public-packets')
    .get(function(req, res) {
        User.find({role: 'wolf'}, function(err, wolves){
            if(err) throw err;
            if(!wolves){
                res.json([]);
            }
            Promise.all(wolves.map(function(wolf){
                return new Promise(function(resolve, reject){
                    SharePacket.find({ownerId: wolf._id, isSelling: true}, function(err, packets){
                        if(err) {
                            reject(err);
                            return;
                        }
                        resolve({
                            username: wolf.username,
                            sellingPackets: packets
                        });
                    });
                });
            })).then(function(sellingPackets){
                var result = [];
                sellingPackets.forEach(function(wolfSellingPacket){
                    result.push(wolfSellingPacket);
                });
                res.json({wolvesPackets: result});
            });
        });
    });

router.route('/reputation')
    .get(function(err, res) {
        User.findOne({role: "cop"}, function(err, cop) {
            if(err) throw err;
            if(!cop) return ;
            res.json({reputation: cop.mixed.reputation});
        });
    });

module.exports = router;