var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var acl = require('express-acl');
var SharePacket = require('../models/sharePacket');
acl.config({
    filename: 'nacl.json',
    baseUrl: 'api'
});

router.use(acl.authorize);

router.route('/')
    .get(function(req, res){

        //get all advertisements from stock market
        SharePacket.find({isSelling: true}, function(req, sellingPackets){
            res.json(sellingPackets);
        });
    })
    .post(function(req, res) {
        SharePacket.findOne({_id: req.body.packetId, isSelling: false}, function(err, packet) {
            if (err) throw err;
            if(!packet) {
                res.json({sucess: false, message: "Пакет акций не найден."});
            }else  {
                var closeTime = Math.floor(Date.now()/1000) + 1800;
                packet.closeTime = closeTime;
                packet.isSelling = true;
                packet.save(function(err, packet) {
                    if(err) throw err;
                    req.io.sockets.emit('broadcast packet added to market', {wolfname: req.username, packet: packet});
                    req.io.sockets.in(req.username).emit('unicast packet added to market', {wolfname: req.username, packet: packet});
                    res.json({success: true, message: "Пакет акций успешно добавлен на фондовый рынок."});
                });
            }
        });
    });

router.route('/take-off')
    .post(function(req, res){
        var packetId = req.body.packetId;
        console.log("About to remove packet " + req.body.packetId);
        SharePacket.findOne({_id: packetId}, function(err, packet) {
            if(err) {
                throw err;
            }
            if(!packet) {
                res.json({success: false, message: "Пакет акций не найден."});
            }else if (packet.isInspected){
                res.json({success: false, message: "Пакет акций проверяется полицейским."});
            }else{
                packet.isSelling = false;
                packet.save(function(err, packet) {
                    if(err) throw err;
                    req.io.sockets.emit('broadcast packet taken off from market', {wolfname: req.username, packetId: packetId});
                    req.io.sockets.in(req.username).emit('unicast packet taken off from market', {packetId: packetId});
                    res.json({success: true, message: "Пакет акций снят с продаж на фондовом рынке."});
                });
            }
        });
    });

module.exports = router;