var express = require('express');
var router = express.Router();
var User = require('../models/user');
var SharePacket = require('../models/sharePacket');
var Fine = require('../models/fine');
var HistoryPacket = require('../models/historyPacket');
var acl = require('express-acl');
var mongoose = require('mongoose');

acl.config({
    filename: 'nacl.json',
    baseUrl: 'api'
});

router.use(acl.authorize);

router.route('/shares')
    .get(function(req, res){
        SharePacket.find({ownerId: req.userId}, function(err, packets){
            if(err) throw err;
            res.json(packets);
        });
    });

router.route('/fines')
    .get(function(req, res) {
        Fine.find({to: req.body.userId}, function(err, bills) {
            if(err) {throw err}
            res.json(bills);
        })
    });

router.route('/balance')
    .get(function(req, res) {
        User.findOne({username: req.username}, function(err, user){
            if(err) throw err;
            if(!user) return;
            res.json({success: true, balance: user.balance});
        });
    });

router.route('/pay-fine')
    .post(function(req, res){
        User.findById(req.user_id, function(err, user){
            if(err) {
                throw err;
            }
            Fine.findById(req.body.billId, function(err, bill){
                if(err) {
                    throw err;
                }
                if(bill.amount > user.balance){
                    res.json({success: false, message: 'У вас недостаточно средств для оплаты штрафа.'});
                }else {
                    Fine.findOneAndRemove({_id: new mongoose.mongo.ObjectID(req.body.billId)},
                        function(err){
                            if(err){
                                throw err;
                            }
                            res.json({
                                success: true,
                                message: "Вы успешно оплатили штраф с id = " + req.body.billId,
                                balance: user.balance});
                        });
                }
            });
        });
    });

router.route('/purchase-shares-packet')
    .post(function(req, res){
        var packetId = req.body.packetId;
        User.findOne({username: req.username}, function(err, user ){
            if(err) {throw err;}
            SharePacket.findById(packetId, function(err, packet){
                if(err) { throw err; }
                if(!packet) {res.json({success: false, message: "Пакет акций не найден."})}
                if(packet.isInspected) {
                    res.json({success: false, message: "Выбранный пакета акций в данный момент проверяется полицейским. Пожалуйста, вернитесь позже."});
                }
                if(user.balance< packet.price) {
                    res.json({success: false, message: "У вас недостаточно средств для покупки данного пакета акций. Пожалуйста, пополните баланс."});
                }else {
                    if(packet.ownerId == null) {
                        user.balance -= packet.price;
                        packet.ownerId = user._id;
                        packet.isSelling = false;
                        packet.save(function(err, packet){
                            if(err)  throw err;
                            user.save(function(err, user){
                                if(err) {throw err;}
                                req.io.sockets.emit('broadcast wolf bought packet', {packetId: packetId, customerName: user.username});
                                req.io.sockets.in(req.username).emit('unicast wolf bought packet', {balance: user.balance, packet: packet});
                                res.json({success: true, balance: user.balance, packet: packet});
                            });
                        });
                    }else {
                        User.findById(packet.ownerId, function(err, seller){
                            if(err) throw err;
                            if(!seller) {
                                res.json({success: false, balance: user.balance, message: "Владелец пакета акций не найден." });
                            }
                            else {
                                seller.balance += packet.price;
                                seller.save(function(err, seller){
                                    if(err) throw err;
                                    user.balance -= packet.price;
                                    user.save(function(err, user){
                                        if(err) throw err;
                                        packet.ownerId = user._id;
                                        packet.isSelling = false;
                                        packet.save(function(err, packet) {
                                            if(err) throw err;
                                            var date = new Date();

                                            date = ("0" + date.getDate()).slice(-2) + "/" + ("0"+(date.getMonth()+1)).slice(-2) + "/" +
                                                date.getFullYear() + " " + ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2);

                                            var sellItem = new HistoryPacket({
                                                packetId: packet._id,
                                                sellerId: seller._id,
                                                company: packet.company,
                                                price: packet.price,
                                                amount: packet.amount,
                                                changed: packet.changed,
                                                isLegal: packet.isLegal,
                                                buyerName: user.username,
                                                date: date,
                                            });
                                            sellItem.save(function(err, sellItem){
                                                if(err) throw err;
                                                //notify seller about his balance
                                                req.io.sockets.in(seller.username).emit('unicast seller packet sold', {packetId: packet._id, balance: seller.balance});
                                                req.io.sockets.in(user.username).emit('unicast wolf about new packet', {packet: packet, balance: user.balance});
                                                req.io.sockets.emit('broadcast wolf bought packet', {packetId: packetId, customerName: user.username});
                                                res.json({success: true, balance: user.balance, packet: packet});
                                            });
                                        });
                                    });
                                });
                            }
                        });
                    }
                }
            });
        });
    });

router.route('/sell-history')
    .get(function(req, res) {
        HistoryPacket.find({sellerId: req.userId}, function(err, items) {
            if(err) throw err;
            if(!items){
                res.json({});
            }
            res.json(items);
        });
    });

module.exports = router;