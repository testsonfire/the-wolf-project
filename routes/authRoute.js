var express = require('express');
var router = express.Router();
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var config = require('../config');
var path = require('path');
var bcrypt = require('bcryptjs');

router.route('/')
    .get(function(req, res){
        res.sendFile(path.join(__dirname, '..', '/static/resources/login.html'));
    })
    .post(function(req, res) {
        console.log(req.body.email);
        console.log(req.body.password);
        User.findOne({email: req.body.email},
            function(err, user){
                if(err) throw err;
                if(!user) {
                    res.json({success: false, message: 'Не удалось авторизоваться. Пользователь не найден.'});
                }else if(user){
                    var hashPassword = bcrypt.hashSync(req.body.password, config.salt);
                    if(user.password != hashPassword) {
                        res.json({
                            success: false,
                            message: 'Не удалось авторизоваться. Такой пользователь не существует, либо пароль неверен.'
                        });
                    }else {
                        var token = jwt.sign(user, config.secret, {
                            expiresIn: 86400 // expires in 24 hours
                        });
                        res.cookie('role',user.role );
                        res.cookie('token', token);
                        res.json({
                            success: true,
                            token: token,
                            username: user.username,
                            email: user.email,
                            userId: user._id,
                            role: user.role,
                            balance: user.balance,
                            redirect: '/' + user.role
                        });
                    }
                }
        });
    });

module.exports = router;