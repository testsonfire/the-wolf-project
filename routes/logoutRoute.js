var express = require('express');
var router = express.Router();
var User = require('../models/user');
var config = require('../config');

router.route('/')
    .post(function(req, res) {
        User.findOne({
            username: req.body.username
        }, function(err, user){
            if(err) throw err;
            res.clearCookie('token');
            res.clearCookie('role');
            res.json({success: true});
        });
    });

module.exports = router;