/**
 * Created by tuyenhuynh on 26/11/16.
 */
var express = require('express');
var jwt = require('jsonwebtoken');
var config = require('../config');
var router = express.Router();

router.use(function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(token) {
        // verifies secret and checks exp
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                return res.json({success: false, message: 'Не удалось авторизовать токен.'});
            } else {
                decoded.role = decoded._doc.role;
                req.decoded = decoded;
                req.username = decoded._doc.username;
                req.userId = decoded._doc._id;
                next();
            }
        });
    }
    else {
        return res.status(403).send({
            success: false,
            message: 'Токен не предоставлен.'
        });
    }
});


module.exports = router;
