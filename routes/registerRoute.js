var express = require('express');
var router = express.Router();
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var config = require('../config');
var bcrypt = require('bcryptjs');

//TODO: remove code repeat
router.route('/')
    .post(function(req, res) {
        User.find({$or: [{'username' : req.body.username}, {'email': req.body.email}]}, function(err, users){
            console.log(req.body.username);
            if(err) throw err;
            if(users.length !==0) {
                console.log("err");
                res.json({success: false, message: 'Выберите другое имя персонажа/email.'});
            }else {
                if(req.body.role ==='cop') {
                    User.findOne({role:'cop'}, function(err, cop) {
                        if(err) {
                            throw err;
                        }
                        if(cop) {
                            res.json({success: false, message: "Пожалуйста, выберите роль волка. Полицейский уже зарегистрирован в системе."});
                        }else {

                            user = new User(req.body);
                            user.mixed = {reputation: 200};
                            user.password = bcrypt.hashSync(req.body.password, config.salt);
                            user.save(function(err, newUser){
                                if(err) {
                                    res.send(err);
                                }else {
                                    var token  = jwt.sign(newUser, config.secret, {
                                        expiresIn: 86400
                                    });
                                    res.cookie('token', token);
                                    res.cookie('role', user.role);
                                    res.json({
                                        success: true,
                                        username: newUser.username,
                                        email: newUser.email,
                                        token : token,
                                        userId: newUser._id,
                                        role: newUser.role,
                                        balance: newUser.balance,
                                        redirect:  newUser.role
                                    });
                                }
                            });
                        }
                    });
                }else  {
                    user = new User(req.body);
                    user.password = bcrypt.hashSync(req.body.password, config.salt);
                    user.save(function(err, newUser){
                        if(err) {
                            res.send(err);
                        }else {
                            var token  = jwt.sign(newUser, config.secret, {
                                expiresIn: 86400
                            });
                            res.cookie('token', token);
                            res.cookie('role', user.role);
                            res.json({
                                success: true,
                                username: newUser.username,
                                email: newUser.email,
                                token : token,
                                userId: newUser._id,
                                role: newUser.role,
                                balance: newUser.balance,
                                redirect:  newUser.role
                            });
                        }
                    });
                }
            }
        });
    });

module.exports = router;