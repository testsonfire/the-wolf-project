var express = require('express');
var router = express.Router();
var config = require('../config');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var User = require('../models/user');
var SharePacket = require('../models/sharePacket');

var clearCookieAndRedirect = function(res) {
    res.clearCookie('token');
    res.clearCookie('role');
    res.redirect('/');
};

router.get('/', function(req, res){
    console.log("Hitting server");

    var role = req.cookies['role'];
    var token = req.cookies['token'];


    if(token) {
        // verifies secret and checks exp
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                console.log("Token out of date");
                res.render("index");
            } else {
                console.log(decoded);
                if (decoded._doc.role == role) {
                    res.redirect(role);
                }
                else {
                    res.render("index");
                }
            }
        });
    }else {
        res.render("index");
    }
});

router.get('/cop', function(req, res) {
    var token = req.cookies['token'];
    if(token && token !== '') {
        jwt.verify(token, config.secret, function (err, decoded) {
            if (!err) {
                User.findOne({username: decoded._doc.username, role: 'cop'}, function(err, user){
                    if(err) throw err;
                    if(!user) {
                        res.clearCookie('token');
                        res.clearCookie('role');
                        res.redirect('/');
                    }else {
                        res.render('cop', {user: user});
                    }
                })}
            else {res.redirect('/');}
        });
    }else {
        res.redirect('/');
    }
});

router.get('/wolf', function(req, res) {
    var token = req.cookies['token'];
    if(token && token !== '') {
        jwt.verify(token, config.secret, function (err, decoded) {
            if(!err) {
                User.findOne({username: decoded._doc.username, role: 'wolf'}, function(err, user){
                    if(err){
                        throw err;
                    }
                    if(user === null) {
                        res.clearCookie('token');
                        res.clearCookie('role');
                        res.redirect('/');
                    }
                    else{
                        //TODO: rewrite this, populate upper
                        SharePacket.find({ownerId: user._id}, function(err, sharePackets){
                            if(err) {
                                throw err;
                            }
                            res.render('wolf-main', {user: user, path: req.path});
                        });
                    }
                });
            }
            else {
                res.redirect('/');
            }
        });
    }else {
        res.redirect('/');
    }
});

router.get('/wolf/market', function(req, res) {
    var token = req.cookies['token'];
    if(token && token !== '') {
        jwt.verify(token, config.secret, function (err, decoded) {
            if(!err) {
                User.findOne({username: decoded._doc.username, role: 'wolf'}, function(err, user){
                    if(err){
                        throw err;
                    }
                    if(user === null) {
                        clearCookieAndRedirect(res);
                    }
                    else{
                        //get all advertisements from stock market
                        SharePacket.find({isSelling: true}, function(err, sellingPackets){
                            if(err) {
                                throw err;
                            }
                            if(!sellingPackets) {
                                sellingPackets = [];
                            }
                            req.io.sockets.in(user.username).emit('send selling packets', sellingPackets);
                            res.render('wolf-market', { path: req.path });
                        });
                    }
                });
            }
            else {
                clearCookieAndRedirect(res);
            }
        });
    }else {
        clearCookieAndRedirect(res);
    }
});


router.get('/wolf/sell-history', function(req, res) {
    var token = req.cookies['token'];
    if(token && token !== '') {
        jwt.verify(token, config.secret, function (err, decoded) {
            if(!err) {
                User.findOne({username: decoded._doc.username, role: 'wolf'}, function(err, user){
                    if(err){
                        throw err;
                    }
                    if(user === null) {
                        clearCookieAndRedirect(res);
                    }
                    else{
                        res.render('wolf-sell-history', { path: req.path });
                    }
                });
            }
            else {
                clearCookieAndRedirect(res);
            }
        });
    }else {
        clearCookieAndRedirect(res);
    }
});



module.exports = router;
