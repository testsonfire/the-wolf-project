var express = require('express');
var router = express.Router();
var User = require('../models/user');
var acl = require('express-acl');
var config  = require('../config');
var bcrypt = require('bcryptjs');

acl.config({
    filename: 'nacl.json',
    baseUrl: 'api'
});

router.use(acl.authorize);

router.route('/:id')
    .get(function(req, res){
        User.findById(req.params.id, function(err, user){
            if(err) {
                res.json(err);
            }else {
                res.json({
                    success: true,
                    username: user.username,
                    email: user.email,
                    userId: user._id,
                    role: user.role,
                    balance: user.balance
                });
            }
        });
    });

router.route('/')
    .post(function(req, res){
        User.findOne({username: req.username}, function (err, user) {
        if (err) {
            res.send(err);
        }
        user.password = bcrypt.hashSync(req.body.password, config.salt);

        user.save(function (err, updatedUser) {
            if (err) {
                res.send(err);
            }
            var result = {
                success: true,
                username: user.username,
                userId: user._id,
                role: user.role,
                balance: user.balance
            };
            res.json(result);
        });
    })})
    .get(function(req, res){
        User.find({}, function(err, users) {
            res.json(users);
        });
    });

router.route('/fines')
    .get(function(req, res){
        Fine.find({userId: req.user_id}, function(err, fines){
            if(err) {
                throw err;
            }
            res.json(fines);
        });
    });


module.exports = router;
