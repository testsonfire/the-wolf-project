
process.env.NODE_ENV = 'test';

var mongoose = require('mongoose');
var User = require('../models/user');
var SharePacket = require('../models/sharePacket');
var Advertisement = require('../models/advertisement');
var chai = require('chai');
var chaiHttp =require('chai-http');
var server = require('../app');
var should = chai.should();
chai.use(chaiHttp);

describe('Advertisements - Wolf', function() {
    var token  = '';
    var userId = '';
    var packetId = '';
    var sharePacket =null;
    before(function(done) {
        SharePacket.remove({}, function(err) {
            if(err) {
                throw err;
            }
            sharePacket = new SharePacket({company: "Facebook", amount: 100, price: 1000, changed: 1.1, isInspected: false, isSelling: false, isLegal: true, closeTime: 1200000});
            sharePacket.save(function(err, sharePacket){
                if(err) {
                    throw err;
                }
                packetId = sharePacket._id;
                User.remove({}, function(err) {
                    if(!err) {
                        chai.request(server)
                            .post('/register')
                            .send({username: 'arthur', password:'password', email: 'arthur@gmail.com', role: 'wolf'})
                            .end(function(err, res){
                                res.status.should.equal(200);
                                res.body.should.be.a('object');
                                res.body.should.have.property('success');
                                res.body.should.have.property('username');
                                res.body.should.have.property('token');
                                res.body.should.have.property('userId');
                                res.body.should.have.property('role');
                                res.body.should.have.property('balance');
                                token = res.body.token;
                                userId = res.body.userId;
                                done();
                            });
                    }
                });
            });
        });
    });

    describe('/POST /api/advertisements', function()  {
        it('It should set status of packet to isSelling', function (done) {
            chai.request(server)
                .post('/api/advertisements')
                .set('x-access-token', token)
                .send({packetId: packetId})
                .end(function (err, res) {
                    //console.log(res.body);
                    res.status.should.equal(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('success');
                    done();
                });
        });
    });

    describe('/POST /api/advertisements/take-off', function()  {
        it('It should return success message', function (done) {
            chai.request(server)
                .post('/api/advertisements/take-off')
                .set('x-access-token', token)
                .send({packetId: packetId})
                .end(function (err, res) {
                    res.status.should.equal(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.should.have.property('message');
                    done();
                });
        });
    });

    describe('/POST /api/advertisements/take-off', function() {
        it('It should return message with success = false', function (done) {
            sharePacket.isInspected = true;
            sharePacket.save(function (err) {
                if (err) {
                    throw err;
                }
                chai.request(server)
                    .post('/api/advertisements/take-off')
                    .set('x-access-token', token)
                    .send({packetId: packetId})
                    .end(function (err, res) {
                        res.status.should.equal(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('success');
                        res.body.should.have.property('message');
                        chai.assert.equal(res.body.success, false);
                        done();
                    });
            });
        });
    });
});

describe('Advertisements - Cop', function() {
    var token = '';
    var userId = '';
    var packetId = '';
    var sharePacket = null;
    before(function (done) {
        SharePacket.remove({}, function (err) {
            if (err) {
                throw err;
            }
            sharePacket = new SharePacket({
                company: "Facebook",
                amount: 100,
                price: 1000,
                changed: 1.1,
                isInspected: false
            });
            sharePacket.save(function (err, sharePacket) {
                if (err) {
                    throw err;
                }
                packetId = sharePacket._id;
            });
        });
        Advertisement.remove({}, function (err) {
            if (err) {
                throw err;
                done();
            }
        });
        User.remove({}, function (err) {
            if (!err) {
                chai.request(server)
                    .post('/register')
                    .send({username: 'arthur', password: 'password', email: 'arthur@gmail.com'})
                    .end(function (err, res) {
                        res.status.should.equal(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('success');
                        res.body.should.have.property('username');
                        res.body.should.have.property('token');
                        res.body.should.have.property('userId');
                        res.body.should.have.property('role');
                        res.body.should.have.property('balance');
                        token = res.body.token;
                        userId = res.body.userId;
                        done();
                    });
            }
        });
    });

});


