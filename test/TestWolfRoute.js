
process.env.NODE_ENV = 'test';

var mongoose = require('mongoose');
var User = require('../models/user');
var Fine = require('../models/fine');
var SharePacket = require('../models/sharePacket');
var chai = require('chai');
var chaiHttp =require('chai-http');
var server = require('../app');
var should = chai.should();
chai.use(chaiHttp);

describe('Test Wolves', function() {
    var wolfToken = '';
    var wolfId = '';
    var packetId = '';
    before(function(done) {
        Fine.remove({}, function(err){
            if(err){
                throw err;
            }
        });
        User.remove({}, function(err) {
            if(!err) {
                //register a wolf
                chai.request(server)
                    .post('/register')
                    .send({username: 'wolf', password:'password', role: 'wolf', email: 'wolf@criminal.com'})
                    .end(function(err, res){
                        res.status.should.equal(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('success');
                        res.body.should.have.property('username');
                        res.body.should.have.property('token');
                        res.body.should.have.property('userId');
                        res.body.should.have.property('role');
                        res.body.should.have.property('balance');
                        wolfToken = res.body.token;
                        wolfId = res.body.userId;
                        sharePacket = new SharePacket({company: "Vk",
                            amount: 1000,
                            price: 10000,
                            changed: 1,
                            isInspected: true,
                            isSelling: true,
                            isLegal: true,
                            closeTime: 1453254543});
                        sharePacket.save(function(err, sharePacket) {
                            if(err) throw err;
                            if(sharePacket) {
                                packetId = sharePacket._id;
                            }
                        })
                        done();
                    });
            }
        });
    });

    //TODO: reset database after testing


    describe('/GET /api/wolf/shares', function()  {
        it('It should return all shares packet of wolf', function (done) {
            chai.request(server)
                .get('/api/wolf/shares')
                .set('x-access-token', wolfToken)
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });

    describe('/GET /api/wolf/sell-history', function()  {
        it('It should return information about all packets that wolf has sold', function (done) {
            chai.request(server)
                .get('/api/wolf/sell-history')
                .set('x-access-token', wolfToken)
                .end(function (err, res) {
                    console.log("-----------------------------------");
                    console.log(res.body);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('historyItems');
                    done();
                });
        });
    });

    describe('/POST /api/wolf/purchase-shares-packet', function()  {
        it('It should return information about all packets that wolf has sold', function (done) {
            chai.request(server)
                .post('/api/wolf/purchase-shares-packet')
                .set('x-access-token', wolfToken)
                .send({packetId: packetId})
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.should.have.property('message');
                    done();
                });
        });
    });

});
