
process.env.NODE_ENV = 'test';

var mongoose = require('mongoose');
var User = require('../models/user');

var chai = require('chai');
var chaiHttp =require('chai-http');
var server = require('../app');
var should = chai.should();
chai.use(chaiHttp);

describe('Test User - Authentication', function() {
    var token  = '';
    before(function(done) {
        User.remove({}, function(err) {
            if(!err) {
                chai.request(server)
                    .post('/register')
                    .send({username: 'arthur', password:'password', email: 'arthur@gmail.com', role: 'wolf'})
                    .end(function(err, res){
                        res.status.should.equal(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('success');
                        res.body.should.have.property('username');
                        res.body.should.have.property('token');
                        res.body.should.have.property('userId');
                        res.body.should.have.property('role');
                        res.body.should.have.property('balance');
                        res.body.should.have.property('redirect');
                        token = res.body.token;
                        done();
                    });
            }
        });
    });

    describe('Test /POST login', function()  {
        it('It should return a user object', function (done) {
            chai.request(server)
                .post('/login')
                .send({email: 'arthur@gmail.com', password: 'password'})
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.should.have.property('token');
                    res.body.should.have.property('username');
                    res.body.should.have.property('userId');
                    res.body.should.have.property('role');
                    res.body.should.have.property('balance');
                    res.body.should.have.property('redirect');
                    done();
                });
        });
    });

    describe('Test /POST login with wrong password', function()  {
        it('It should return an error', function (done) {
            chai.request(server)
                .post('/login')
                .send({username: 'arthur@gmail.com', password: 'arthur'})
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.have.property('success');
                    res.body.should.have.property('message');
                    done();
                });
        });
    });

});
