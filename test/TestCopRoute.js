process.env.NODE_ENV = 'test';

var mongoose = require('mongoose');
var User = require('../models/user');
var Fine = require('../models/fine');
var SharePacket = require('../models/sharePacket');
var chai = require('chai');
var chaiHttp =require('chai-http');
var server = require('../app');
var should = chai.should();
chai.use(chaiHttp);

describe('Test User Cop', function() {
    var copToken  = '';
    var wolfToken = '';
    var wolfId = '';
    var sharePacket = null;
    var sharePacketId = null;
    before(function(done) {
        Fine.remove({}, function(err){
            if(err){
                console.log(err);
            }
        });
        User.remove({}, function(err) {
            if(!err) {
                //register a wolf
                chai.request(server)
                    .post('/register')
                    .send({username: 'wolf', password:'password', email: 'wolf@gmail.com',  role: 'wolf'})
                    .end(function(err, res){
                        res.status.should.equal(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('success');
                        res.body.should.have.property('username');
                        res.body.should.have.property('token');
                        res.body.should.have.property('userId');
                        res.body.should.have.property('role');
                        res.body.should.have.property('balance');
                        wolfToken = res.body.token;
                        wolfId = res.body.userId;
                        //register a cop
                        chai.request(server)
                            .post('/register')
                            .send({username: 'cop', password:'password', email: 'arthur@gmail.com', role: 'cop'})
                            .end(function(err, res){
                                res.status.should.equal(200);
                                res.body.should.be.a('object');
                                res.body.should.have.property('success');
                                res.body.should.have.property('username');
                                res.body.should.have.property('token');
                                res.body.should.have.property('userId');
                                res.body.should.have.property('role');
                                res.body.should.have.property('balance');
                                //TODO : register role
                                copToken = res.body.token;
                                sharePacket = new SharePacket({ownerId: wolfId, company: "Vk", amount: 1000, price: 10000, changed: 1, isInspected: true, isSelling: true, isLegal: true,  closeTime: 1453254543});
                                sharePacket.save(function(err, newSharePacket) {
                                    if(err){
                                        throw err;
                                    }
                                    sharePacketId = newSharePacket._id;
                                });
                                done();
                            });
                    });
            }
        });
    });

    //TODO: run this test
    describe('/GET /api/cop/wolf-public-packets', function()  {
        it('It should all public packets on market, group by wolf', function (done) {
            chai.request(server)
                .get('/api/cop/wolf-public-packets')
                .set('x-access-token', copToken)
                .end(function (err, res) {
                    console.log(res.body);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });

    describe('/POST /api/cop/inspect', function()  {
        it('It should return a success message', function (done) {
            chai.request(server)
                .post('/api/cop/inspect')
                .set('x-access-token', copToken)
                .send({packetId: sharePacketId})
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('success');
                    done();
                });
        });
    });

    describe('/POST /api/cop/finish-inspect', function()  {
        it('Should return a success message', function (done) {
            chai.request(server)
                .post('/api/cop/finish-inspect')
                .set('x-access-token', copToken)
                .send({packetId: sharePacketId})
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('success');
                    done();
                });
        });
    });

    describe('/GET /api/cop/reputation', function()  {
        it('Should return a success message with reputation', function (done) {
            chai.request(server)
                .get('/api/cop/reputation')
                .set('x-access-token', copToken)
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('reputation');
                    done();
                });
        });
    });


    //TODO: test make fine

});
