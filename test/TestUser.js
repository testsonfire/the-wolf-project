
process.env.NODE_ENV = 'test';

var mongoose = require('mongoose');
var User = require('../models/user');

var chai = require('chai');
var chaiHttp =require('chai-http');
var server = require('../app');
var should = chai.should();
chai.use(chaiHttp);


describe('Test Users', function() {
    var token  = '';
    before(function(done) {
        User.remove({}, function(err) {
            if(!err) {
                chai.request(server)
                    .post('/register')
                    .send({username: 'arthur', password:'password', role: 'cop', email: "fdaf@fdfdsa"})
                    .end(function(err, res){
                        res.status.should.equal(200);

                        res.body.should.be.a('object');
                        res.body.should.have.property('success');
                        res.body.should.have.property('username');
                        res.body.should.have.property('token');
                        res.body.should.have.property('userId');
                        res.body.should.have.property('role');
                        res.body.should.have.property('balance');
                        token = res.body.token;
                        done();
                    });
            }
        });
    });

    describe('/GET users', function()  {
        it('It should GET all the users', function (done) {
            chai.request(server)
                .get('/api/users')
                .set('x-access-token', token)
                .end(function (err, res) {
                    res.status.should.equal(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(1);
                    done();
                });
        });
    });
});

describe ('Updates user', function(){
    var token  = '';
    before(function(done) {
        User.remove({}, function(err) {
            if(!err) {
                chai.request(server)
                    .post('/register')
                    .send({username: 'arthur', password:'password', role: 'wolf', email: 'afda2d@mfdafds'})
                    .end(function(err, res){
                        res.status.should.equal(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('success');
                        res.body.should.have.property('username');
                        res.body.should.have.property('token');
                        res.body.should.have.property('userId');
                        res.body.should.have.property('role');
                        res.body.should.have.property('balance');
                        token = res.body.token;
                        done();
                    });
            }
        });
    });

    describe('/POST users', function() {
        it('It return updated version of current user', function (done) {
            //TODO: restrict field, allow update only password
            //TODO: update token from server
            var user = {
                password: 'new-password'
            };
            chai.request(server)
                .post('/api/users')
                .set('x-access-token', token)
                .send(user)
                .end(function (err, res) {
                    res.status.should.equal(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.should.have.property('username');
                    res.body.should.have.property('userId');
                    res.body.should.have.property('role');
                    res.body.should.have.property('balance');
                    done();
                });
        });
    });
});
